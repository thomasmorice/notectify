# Notectify 

<img height="300px" src="./public/gitlab-content/app-screenshot.png">

[![Netlify Status](https://api.netlify.com/api/v1/badges/affc4066-aa50-4ce5-acba-8c0c3f16e4ef/deploy-status)](https://app.netlify.com/sites/notectify/deploys)

## About the project 
Build a note-taking app. Think of it as a web-based OSX note taking app.

I choosed to work without any css framework, but using scss. This because I love working with design in my spare time 😊.

I started by creating some design under Figma, working with some visual is way easier than without. 

The design is flexible, and once you go over iPad width, the split screen becomes vertical. 

## Some requirements
- The user should be able to create a new note.
- The user should be able to edit and delete a note.
- The user should be able to navigate through multiple notes.
- Search function to find notes.

## How can I see it
There are two ways to visualize this project: 
- A live version is hosted in <a href="https://notectify.netlify.com"> netlify </a>
- Clone this project locally, and run `npm install && npm run start` or `yarn && yarn start`
