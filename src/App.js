import React from 'react';
import './App.scss';
import NoteEditor from './NoteEditor/NoteEditor';
import NoteList from './NoteList/NoteList';

const notectifyMode = {
  writing: 'writing-mode',
  searching: 'searching-mode'
}

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      noteId: 2,
      noteList: [],
      mode: notectifyMode.writing,
      noteContent: ''
    };
    this.noteEditor = React.createRef();
  }

  setMode = (mode) => {
    this.setState({
      mode: mode,
    })
  }
  
  updateNote = (newNote) => {
    if (!newNote.id) {
      // create a new note
      newNote.id = this.state.noteId
      this.setState(prevState => ({
        mode: notectifyMode.searching,
        noteList: [...prevState.noteList, newNote],
        noteId: this.state.noteId + 1
      }))
    } else {
      // Modify an existing note
      const newNoteList = this.state.noteList.map(note => note.id === newNote.id ? newNote : note);
      this.setState(prevState => ({
        mode: notectifyMode.searching,
        noteList: [...newNoteList],
      }))
    }

  }

  openNote = (id) => {
    const note = this.state.noteList.find(note => note.id === id);
    this.noteEditor.current.openNote(note)
  }

  deleteNote = (id) => {
    const newNoteList = this.state.noteList.filter(note => note.id !== id);

    this.setState({
      mode: newNoteList.length === 0 ? notectifyMode.writing : notectifyMode.searching,
      noteList: [...newNoteList]
    })

  }


  render() {
    return (
      <div className="App">
        <header className="header">
          <h1> Notectify </h1>
        </header>
        <div className={`note-container ${this.state.mode}`}>
          <NoteEditor 
            ref={this.noteEditor}
            onFocus={() => this.setMode(notectifyMode.writing)} 
            onBlur={() => this.state.noteList.length !== 0 ? this.setMode(notectifyMode.searching) : this.setMode(notectifyMode.writing)}
            onNewNoteSubmitted={this.updateNote}
          />
          <NoteList 
            onOpenNote={(id) => {this.openNote(id)}}
            onDeleteNote={(id) => {this.deleteNote(id)}}
            noteList={this.state.noteList}
            onSearching={() => {this.setMode(notectifyMode.searching)}}
          />
        </div>
      </div>
    )
  }
}

export default App;
