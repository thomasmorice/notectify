import React from 'react';
import './NoteEditor.scss';

class NoteEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noteId: null,
      noteTitle: '',
      noteContent: '',
      errorMessage: ''
    };
  }

  changeToSearchMode = () => {
    // Only switch to search mode if the editors input are empty
    if (this.state.noteContent === '' && this.state.noteTitle === '') {
      this.props.onBlur()
    }
  }

  updateTitle = (value) => {
    this.setState({
      errorMessage: '',
      noteTitle: value
    });
  }

  updateContent = (value) => {
    this.setState({
      errorMessage: '',
      noteContent: value
    });
  }

  openNote = (note) => {
    this.setState({
      noteId: note.id,
      noteTitle: note.title,
      noteContent: note.content,
    })
    let textAreaElement = document.querySelector('.textarea-content')
    textAreaElement.focus()
    textAreaElement.selectionStart = textAreaElement.selectionEnd = textAreaElement.value.length
  }

  saveNote = () => {
    if (this.state.noteTitle === '') {
      this.setState({
        errorMessage: 'Please add a title'
      });
    } else if (this.state.noteContent === '') {
      this.setState({
        errorMessage: 'The content of the note is empty...'
      })
    } else {
      // All fields are correct, save the note
      this.props.onNewNoteSubmitted({
        id: this.state.noteId,
        title: this.state.noteTitle,
        content: this.state.noteContent
      })
      this.setState({
        noteId: null,
        noteTitle: '',
        noteContent: '',
        errorMessage: ''
      })
    }
  }

  render() {
    return (
      <div className="Note-editor"> 
        {this.state.errorMessage !== '' && 
          <div className="error-message"> {this.state.errorMessage} </div>
        }
        
        <input className="input-title" type="text" placeholder="Title" 
          value={this.state.noteTitle} 
          onChange={e => this.updateTitle(e.target.value)}
          onFocus={this.props.onFocus}
          onBlur={this.changeToSearchMode}
        />
        <textarea className="textarea-content" placeholder="start writing your note..." 
          value={this.state.noteContent} 
          onChange={e => this.updateContent(e.target.value)}
          onFocus={this.props.onFocus} 
          onBlur={this.changeToSearchMode}
        >
        </textarea>
        {(this.state.noteTitle || this.state.noteContent) &&
          <div className="note-actions"> 
            <button onClick={this.saveNote} className="btn-ghost">save</button> 
          </div>
        }
      </div>
    )
  }
}

export default NoteEditor;