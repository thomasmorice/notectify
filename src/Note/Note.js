import React from 'react';

import './Note.scss'

class Note extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      title: props.title,
      content: props.content
    };
  }

  componentDidUpdate() {
    if (this.state.title !== this.props.title) {
      this.setState({
        title: this.props.title
      })
    } else if (this.state.content !== this.props.content) {
      this.setState({
        content: this.props.content
      })
    }
  }  

  render() {
    return ( 
      <div className="Note">
        <div onClick={() => this.props.onOpenNote(this.state.id)} className="note-actions edit-note"></div>
        <div className="note-title">{this.state.title}</div>
        <span onClick={() => this.props.onDeleteNote(this.state.id)} className="note-actions delete-note"></span>
      </div>
    )
  }
}

export default Note