import React from 'react';
import './NoteList.scss';

import Note from '../Note/Note'

class NoteList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noteList: props.noteList,
      searchNoteValue: ''
    };
  }

  componentDidUpdate() {
    if (this.state.noteList !== this.props.noteList) {
      this.setState({
        noteList: this.props.noteList
      })
    }
  }

  notesList = () => {
    return (
      <div className="notes">
        {this.state.noteList.map((note, index) => {
          if (note.title.toLowerCase().includes(this.state.searchNoteValue.toLowerCase())) {
            return (
              <Note
                id={note.id}
                key={note.id}
                title={note.title}
                content={note.content}
                onOpenNote={(id) => this.props.onOpenNote(id)}
                onDeleteNote={(id) => this.props.onDeleteNote(id)}
              />
            )
          } else {
            return null
          }
        })}
      </div>
    );
  }

  searchNotes = (value) => {
    if (value !== '') {
      this.props.onSearching()
    }
    this.setState({
      searchNoteValue: value
    }); 
  }

  render() {
    return (
      <div className="Note-list"> 
        {this.state.noteList.length === 0 ?
          <div className="empty-note-list"> you don’t have any note yet... </div>
        :
          <div className="note-list-wrapper">
            <input className="input-search-note" onChange={(e) => this.searchNotes(e.target.value)} value={this.state.searchNoteValue} type="text" placeholder="search for a note" /> 
            <div className="notes"> 
              { this.notesList() }
            </div>
          </div>
        }
        </div>
    )
  }
}

export default NoteList;